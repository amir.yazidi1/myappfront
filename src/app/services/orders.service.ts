import { Injectable } from '@angular/core';
import {Order} from "../modeles/order";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  listOrdersShare: Order[];

  constructor(private http : HttpClient) { }

  getOrdersWS(){
    return this.http.get<[]>( environment.baseUrl+"orders/");
  }
  deleteOrder(id){
    return this.http.delete(environment.baseUrl+"orders/?id=" + id);
  }
  postOrder(order: Order){
    return this.http.post(environment.baseUrl+"orders/", order);
  }
  putOrder(order: Order){
    return this.http.put(environment.baseUrl+"orders/", order);
  }
  getOrderBy(id){
    return this.http.get(environment.baseUrl+"orders/" + id);
  }
}
