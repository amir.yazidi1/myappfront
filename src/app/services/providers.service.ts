import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Provider} from "../modeles/provider";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProvidersService {
  listProviderShare: Provider[];

  constructor(private http : HttpClient) { }
  getProviderWS(){
    return this.http.get<Provider[]>( environment.baseUrl+"Providers/");
  }
  deleteProvider(id){
    return this.http.delete(environment.baseUrl+"Providers/?id=" + id);
  }
  postProvider(provider: Provider){
    return this.http.post(environment.baseUrl+"Providers/", provider);
  }
  putProvider(provider: Provider){
    return this.http.put(environment.baseUrl+"Providers/", provider);
  }
  getProviderBy(id){
    return this.http.get(environment.baseUrl+"Providers/" + id);
  }
}
