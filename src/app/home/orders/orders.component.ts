import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {OrdersService} from "../../services/orders.service";
import {Order} from "../../modeles/order";

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {
  ordors: Order[]

  constructor(private OrderService: OrdersService) { }

  ngOnInit(): void {


    this.getAllOrders();
  }

  getAllOrders(){

    this.OrderService.getOrdersWS().subscribe(res => {
      this.ordors = res;
    });
  }
  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {this.OrderService.deleteOrder(id).subscribe(
        () => {


          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.getAllOrders();

        }
        //  this.ListClients = this.ListClients.filter(client => client.id != id)


      );

      }
    });
  }
}
