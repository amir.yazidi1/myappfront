import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {SubCategory} from "../modeles/sub-category";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class SubCatogoriesService {
  listSubCategoriesShare: SubCategory[];

  constructor(private  http : HttpClient) { }
  getSubCategoriesWS(){
    return this.http.get<SubCategory[]>( environment.baseUrl+"subCategory/");
  }
  deleteSubCategory(id){
    return this.http.delete(environment.baseUrl+"subCategory/?id=" + id);
  }
  postSubCategory(subCategory: SubCategory){
    return this.http.post(environment.baseUrl+"subCategory/", subCategory);
  }
  putSubCategory(subCategory: SubCategory){
    return this.http.put(environment.baseUrl+"subCategory/", subCategory);
  }

  getSubCategoryBy(id){
    return this.http.get(environment.baseUrl+"subCategory/" + id);
  }
}
