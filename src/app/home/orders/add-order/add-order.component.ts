import { Component, OnInit } from '@angular/core';
import {OrdersService} from "../../../services/orders.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {
  formOrder: FormGroup;
  submitted = false;
  constructor(private OrderService: OrdersService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formOrder = this.fb.group({
      name: ['', Validators.required],
      totalPrice: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  get f() { return this.formOrder.controls; }
  addOrder(){
    console.log(this.formOrder.value)
    this.submitted = true;
    if (this.formOrder.invalid) {return ; }
    this.OrderService.postOrder(this.formOrder.value).subscribe()
  }
}
