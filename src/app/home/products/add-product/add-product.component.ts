import { Component, OnInit } from '@angular/core';
import {ProductsService} from "../../../services/products.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {SubCatogoriesService} from "../../../services/sub-catogories.service";

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.component.html',
  styleUrls: ['./add-product.component.css']
})
export class AddProductComponent implements OnInit {
  formProduct: FormGroup;
  submitted = false;
  subcategories:any=[];
  constructor(private productsService: ProductsService,private subCatogoriesService:SubCatogoriesService, private fb: FormBuilder) {

    this.subCatogoriesService.getSubCategoriesWS().subscribe(res=> {
      console.log(res)
      this.subcategories=res;
    })



  }

  ngOnInit(): void {
    this.formProduct = this.fb.group({
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required],
      subcat: ['', Validators.required]
    })



  }

  get f() { return this.formProduct.controls; }
  AddProduct(){


    console.log(this.formProduct.value)
    this.submitted = true;
    if (this.formProduct.invalid) {return ; }
    this.productsService.postProduct(this.formProduct.value).subscribe()}
}
