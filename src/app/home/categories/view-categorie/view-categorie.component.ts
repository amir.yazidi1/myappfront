import { Component, OnInit } from '@angular/core';
import {CategoriesService} from "../../../services/categories.service";

@Component({
  selector: 'app-view-categorie',
  templateUrl: './view-categorie.component.html',
  styleUrls: ['./view-categorie.component.css']
})
export class ViewCategorieComponent implements OnInit {

  constructor(private categoriesService: CategoriesService) { }

  ngOnInit(): void {
  }

}
