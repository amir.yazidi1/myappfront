import {Component, OnInit} from '@angular/core';
import Swal from 'sweetalert2';
import {ClientsService} from "../../services/clients.service";
import {Client} from "../../modeles/client";

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.css']
})
export class ClientComponent implements OnInit {
  client: Client[]
  term;

  constructor(private clientService: ClientsService) {
  }

  ngOnInit(): void {


    this.getAllClients();
  }

  getAllClients() {

    this.clientService.getClientsWS().subscribe(res => {
      this.client = res;
    });
  }

  delete(id) {

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {
        this.clientService.deleteClient(id).subscribe(
          () => {


            Swal.fire(
              'Deleted!',
              'Your file has been deleted.',
              'success'
            );

            this.getAllClients();

          }
          //  this.ListClients = this.ListClients.filter(client => client.id != id)


        );

      }
    });
  }

}
