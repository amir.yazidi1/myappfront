import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomeComponent} from "./home/home.component";
import {LoginComponent} from "./auth/login/login.component";
import {RegisterComponent} from "./auth/register/register.component";
import {ForgetPasswordComponent} from "./auth/forget-password/forget-password.component";
import {ErrorComponent} from "./error/error.component";
import {LayoutComponent} from "./home/layout/layout.component";
import {ClientComponent} from "./home/client/client.component";
import {ProviderComponent} from "./home/provider/provider.component";
import {OrdersComponent} from "./home/orders/orders.component";
import {CategoriesComponent} from "./home/categories/categories.component";
import {SubCategoriesComponent} from "./home/sub-categories/sub-categories.component";
import {AddClientComponent} from "./home/client/add-client/add-client.component";
import {AddProviderComponent} from "./home/provider/add-provider/add-provider.component";
import {AddOrderComponent} from "./home/orders/add-order/add-order.component";
import {AddCategorieComponent} from "./home/categories/add-categorie/add-categorie.component";
import {AddSubCategorieComponent} from "./home/sub-categories/add-sub-categorie/add-sub-categorie.component";
import {AuthGuard} from "./guards/auth.guard";
import {EditSubCategorieComponent} from "./home/sub-categories/edit-sub-categorie/edit-sub-categorie.component";
import {EditCategorieComponent} from "./home/categories/edit-categorie/edit-categorie.component";
import {EditClientComponent} from "./home/client/edit-client/edit-client.component";
import {EditProvidersComponent} from "./home/provider/edit-providers/edit-providers.component";
import {EditOrderComponent} from "./home/orders/edit-order/edit-order.component";
import {ProductsComponent} from "./home/products/products.component";
import {AddProductComponent} from "./home/products/add-product/add-product.component";
import {EditProductComponent} from "./home/products/edit-product/edit-product.component";
import {ViewCategorieComponent} from "./home/categories/view-categorie/view-categorie.component";
import {ViewClientComponent} from "./home/client/view-client/view-client.component";
import {ViewProviderComponent} from "./home/provider/view-provider/view-provider.component";
import {ViewProductComponent} from "./home/products/view-product/view-product.component";
import {ViewSubcategorieComponent} from "./home/sub-categories/view-subcategorie/view-subcategorie.component";
import {ViewOrderComponent} from "./home/orders/view-order/view-order.component";


const routes: Routes = [

  {
    path: "", component: HomeComponent, canActivate:[AuthGuard],
    children: [

      {path: "", component: LayoutComponent},
      {path: "clients", component: ClientComponent},
      {path: "providers", component: ProviderComponent},
      {path: "orders", component: OrdersComponent},
      {path: "categories", component: CategoriesComponent},
      {path: "subCategories", component: SubCategoriesComponent},
      {path: "products", component: ProductsComponent},
      {path: "addProduct", component: AddProductComponent},
      {path: "addClient", component: AddClientComponent},
      {path: "addProvider", component: AddProviderComponent},
      {path: "addOrder", component: AddOrderComponent},
      {path: "addCategorie", component: AddCategorieComponent},
      {path: "addSubCategorie", component: AddSubCategorieComponent},
      {path: "editSubCategorie/:id", component: EditSubCategorieComponent},
      {path: "editCategorie/:id", component: EditCategorieComponent},
      {path: "editClient/:id", component: EditClientComponent},
      {path: "editProvider/:id", component: EditProvidersComponent},
      {path: "editProduct/:id", component: EditProductComponent},
      {path: "editOrder/:id", component: EditOrderComponent},
      {path: "viewCategorie", component: ViewCategorieComponent},
      {path: "viewSubCategorie", component: ViewSubcategorieComponent},
      {path: "viewClient", component: ViewClientComponent},
      {path: "viewProvider", component: ViewProviderComponent},
      {path: "viewProduct", component: ViewProductComponent},
      {path: "viewOrder", component: ViewOrderComponent},

    ]
  },
  {path: "login", component: LoginComponent},
  {path: "register", component: RegisterComponent},
  {path: "forget", component: ForgetPasswordComponent},
  {path: "**", component: ErrorComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
