import { Component, OnInit } from '@angular/core';
import {SubCatogoriesService} from "../../../services/sub-catogories.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-sub-categorie',
  templateUrl: './add-sub-categorie.component.html',
  styleUrls: ['./add-sub-categorie.component.css']
})
export class AddSubCategorieComponent implements OnInit {
  formSubCategory: FormGroup;
  submitted = false;
  constructor(private subCatogoriesService: SubCatogoriesService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formSubCategory = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  get f() { return this.formSubCategory.controls; }
  AddSubCategory(){


    console.log(this.formSubCategory.value)
    this.submitted = true;
    if (this.formSubCategory.invalid) {return ; }
    this.subCatogoriesService.postSubCategory(this.formSubCategory.value).subscribe()}
}
