import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewSubcategorieComponent } from './view-subcategorie.component';

describe('ViewSubcategorieComponent', () => {
  let component: ViewSubcategorieComponent;
  let fixture: ComponentFixture<ViewSubcategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewSubcategorieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewSubcategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
