import { Component, OnInit } from '@angular/core';
import {CategoriesService} from "../../../services/categories.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-categorie',
  templateUrl: './edit-categorie.component.html',
  styleUrls: ['./edit-categorie.component.css']
})
export class EditCategorieComponent implements OnInit {
  formCategory: FormGroup;
  submitted = false;
  id;
  category;
  constructor(private categoriesService: CategoriesService, private fb: FormBuilder, private router: ActivatedRoute) {
    this.id = this.router.snapshot.params.id;
  }
  ngOnInit(): void {
    this.formCategory = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.getById()

  }
  get f() {
    return this.formCategory.controls;
  }

  updateCategory() {
    console.log(this.formCategory.value)
    this.submitted = true;
    if (this.formCategory.invalid) {
      return;
    }
    this.categoriesService.putCategory(this.formCategory.value).subscribe()

  }

  async  getById() {
    await this.categoriesService.getCategoryBy(this.id).subscribe(res => {

      console.log("res: ", res["name"])
      this.category = res

      this.formCategory.setValue({
        id: this.id,
        name: res["name"],
        description: res ["description"]
      })
    })
  }

}
