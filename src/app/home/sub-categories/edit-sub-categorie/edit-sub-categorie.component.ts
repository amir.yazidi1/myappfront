import {Component, OnInit} from '@angular/core';
import {SubCatogoriesService} from "../../../services/sub-catogories.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-sub-categorie',
  templateUrl: './edit-sub-categorie.component.html',
  styleUrls: ['./edit-sub-categorie.component.css']
})
export class EditSubCategorieComponent implements OnInit {
  formSubCategory: FormGroup;
  submitted = false;
  id;
  client;

  constructor(private subCatogoriesService: SubCatogoriesService, private fb: FormBuilder, private router: ActivatedRoute) {
    this.id = this.router.snapshot.params.id;

  }

  ngOnInit(): void {
    this.formSubCategory = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.getById()


  }

  get f() {
    return this.formSubCategory.controls;
  }

  updateSubCategory() {


    console.log(this.formSubCategory.value)
    this.submitted = true;
    if (this.formSubCategory.invalid) {
      return;
    }
    this.subCatogoriesService.putSubCategory(this.formSubCategory.value).subscribe()

  }

  async getById() {
    await this.subCatogoriesService.getSubCategoryBy(this.id).subscribe(res => {

      console.log("res : ", res["name"])
      this.client = res

      this.formSubCategory.setValue({
        id: this.id,
        name: res["name"],
        description: res["description"],
      })


    })
  }

}
