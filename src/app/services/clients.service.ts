import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {Client} from "../modeles/client";

@Injectable({
  providedIn: 'root'
})
export class ClientsService {
  listClientShare: Client[];
  constructor(private http: HttpClient) { }

  getClientsWS(){
    return this.http.get<Client[]>( environment.baseUrl+"clients/");
  }
  deleteClient(id){
    return this.http.delete(environment.baseUrl+"clients/?id=" + id);
  }
  postClient(client){
    return this.http.post(environment.baseUrl+"clients/", client);
  }
  putClient(client: Client){
    return this.http.put(environment.baseUrl+"clients/", client);
  }

  getClientBy(id){
    return this.http.get(environment.baseUrl+"clients/" + id);
  }
}

