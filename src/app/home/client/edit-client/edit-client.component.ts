import {Component, OnInit} from '@angular/core';
import {ClientsService} from "../../../services/clients.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-edit-client',
  templateUrl: './edit-client.component.html',
  styleUrls: ['./edit-client.component.css']
})
export class EditClientComponent implements OnInit {
  formClient: FormGroup;
  submitted = false;
  id;
  client;

  constructor(private ClientService: ClientsService, private fb: FormBuilder,
              private routerr: Router, private router: ActivatedRoute, private toastr: ToastrService) {

    this.id = this.router.snapshot.params.id;
  }

  ngOnInit(): void {
    this.formClient = this.fb.group({
      id: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      photo: ['', Validators.required],
      adress: ['', Validators.required]
    });
    this.getById()


  }

  get f() {
    return this.formClient.controls;
  }

  updateClient() {


    console.log(this.formClient.value)
    this.submitted = true;
    if (this.formClient.invalid) {
      return;
    }
    this.ClientService.putClient(this.formClient.value).subscribe(
      res => {
        this.toastr.success('Client update successfully');
        this.routerr.navigate(["/clients"])
      })

  }

   async  getById() {
   await this.ClientService.getClientBy(this.id).subscribe(res => {

      console.log("res : ", res["firstName"])
      this.client = res

     this.formClient.setValue({
        id:this.id,
       firstName:res["firstName"],
       lastName:res["lastName"],
       phone:res["phone"],
       photo:res["photo"],
       adress:res["adress"],
     })


    })
  }

}
