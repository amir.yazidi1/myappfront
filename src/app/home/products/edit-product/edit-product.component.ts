import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../../../services/products.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.css']
})
export class EditProductComponent implements OnInit {
  formProduct: FormGroup;
  submitted = false;
  id;
  product;

  constructor(private productsService: ProductsService, private fb: FormBuilder, private router: ActivatedRoute) {
    this.id = this.router.snapshot.params.id;
  }

  ngOnInit(): void {
    this.formProduct = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      price: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.getById()


  }

  get f() {
    return this.formProduct.controls;
  }

  updateProduct() {


    console.log(this.formProduct.value)
    this.submitted = true;
    if (this.formProduct.invalid) {
      return;
    }
    this.productsService.putProduct(this.formProduct.value).subscribe()

  }

  async getById() {
    await this.productsService.getProductBy(this.id).subscribe(res => {

      console.log("res : ", res["name"])
      this.product = res

      this.formProduct.setValue({
        id: this.id,
        name: res["name"],
        price: res["price"],
        description: res["description"],
      })
    })
  }

}
