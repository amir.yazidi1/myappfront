import { Component, OnInit } from '@angular/core';
import {ClientsService} from "../../../services/clients.service";

@Component({
  selector: 'app-view-client',
  templateUrl: './view-client.component.html',
  styleUrls: ['./view-client.component.css']
})
export class ViewClientComponent implements OnInit {

  constructor(private ClientService: ClientsService) { }

  ngOnInit(): void {
  }

}
