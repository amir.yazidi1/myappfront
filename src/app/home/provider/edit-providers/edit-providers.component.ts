import { Component, OnInit } from '@angular/core';
import {ProvidersService} from "../../../services/providers.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-providers',
  templateUrl: './edit-providers.component.html',
  styleUrls: ['./edit-providers.component.css']
})
export class EditProvidersComponent implements OnInit {
  formProvider: FormGroup;
  submitted = false;
  id;
  client
  constructor(private providersService: ProvidersService, private fb: FormBuilder, private router: ActivatedRoute) {
    this.id = this.router.snapshot.params.id;

  }

  ngOnInit(): void {
    this.formProvider = this.fb.group({
      id: [''],
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      photo: ['', Validators.required],
      campany: ['', Validators.required]
    });
    this.getById()


  }

  get f() {
    return this.formProvider.controls;
  }

  updateProvider() {


    console.log(this.formProvider.value)
    this.submitted = true;
    if (this.formProvider.invalid) {
      return;
    }
    this.providersService.putProvider(this.formProvider.value).subscribe()

  }

  async  getById() {
    await this.providersService.getProviderBy(this.id).subscribe(res => {

      console.log("res : ", res["firstName"])
      this.client = res

      this.formProvider.setValue({
        id:this.id,
        firstName:res["firstName"],
        lastName:res["lastName"],
        phone:res["phone"],
        photo:res["photo"],
        campany:res["campany"],
      })


    })
  }
}
