import { TestBed } from '@angular/core/testing';

import { SubCatogoriesService } from './sub-catogories.service';

describe('SubCatogoriesService', () => {
  let service: SubCatogoriesService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SubCatogoriesService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
