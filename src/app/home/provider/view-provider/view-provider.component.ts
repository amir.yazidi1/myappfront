import { Component, OnInit } from '@angular/core';
import {ProvidersService} from "../../../services/providers.service";

@Component({
  selector: 'app-view-provider',
  templateUrl: './view-provider.component.html',
  styleUrls: ['./view-provider.component.css']
})
export class ViewProviderComponent implements OnInit {

  constructor(private providersService: ProvidersService) { }

  ngOnInit(): void {
  }

}
