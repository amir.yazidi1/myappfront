export class Order {
  id: number;
  name: string;
  lastName: string;
  totalPrice: string;
  description: string;
}
