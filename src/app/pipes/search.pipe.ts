import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'search'
})
export class SearchPipe implements PipeTransform {

  transform(items: any[],term:String): any  {

       console.log(term)
       console.log(items)
    if (!items || !term) {
      return items;
    }

    return items.filter(e=> e.firstName.includes(term) || e.lastName.includes(term) ) ;
  }

}
