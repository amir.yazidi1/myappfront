import {Component, OnInit} from '@angular/core';
import {ClientsService} from "../../../services/clients.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-client',
  templateUrl: './add-client.component.html',
  styleUrls: ['./add-client.component.css']
})
export class AddClientComponent implements OnInit {
  formClient: FormGroup;
  submitted = false;
  fileToUpload: File = null;

  constructor(private ClientService: ClientsService, private fb: FormBuilder, private router: Router, private toastr: ToastrService) {
  }


  ngOnInit(): void {
    this.formClient = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      photo: ['', Validators.required],
      adress: ['', Validators.required]
    });
  }

  get f() {
    return this.formClient.controls;
  }

  AddClient() {

    console.log(this.formClient.value)
    this.submitted = true;
    if (this.formClient.invalid) {
      return;
    }

    const formData = new FormData();
    formData.append('img', this.fileToUpload)
    formData.append('firstName', this.formClient.get("firstName").value)
    formData.append('lastName', this.formClient.get("lastName").value)
    formData.append('phone', this.formClient.get("phone").value)
    formData.append('adress', this.formClient.get("adress").value)


    this.ClientService.postClient(formData).subscribe(res => {
      this.toastr.success('Client add successfully');
      this.router.navigate(["/clients"])
    })
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

}
