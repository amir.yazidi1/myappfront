import { Component, OnInit } from '@angular/core';
import {OrdersService} from "../../../services/orders.service";

@Component({
  selector: 'app-view-order',
  templateUrl: './view-order.component.html',
  styleUrls: ['./view-order.component.css']
})
export class ViewOrderComponent implements OnInit {

  constructor(private OrderService: OrdersService) { }

  ngOnInit(): void {
  }

}
