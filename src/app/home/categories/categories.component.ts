import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {CategoriesService} from "../../services/categories.service";
import {Category} from "../../modeles/category";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit {

  categories: Category[]
  constructor(private CategoriesService: CategoriesService) { }

  ngOnInit(): void {
    this.getAllCategories();
  }

  getAllCategories(){

    this.CategoriesService.getCategoriessWS().subscribe(res => {
      this.categories = res;
    });
  }
  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {this.CategoriesService.deleteCategory(id).subscribe(
        () => {


          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.getAllCategories();

        }
        //  this.ListClients = this.ListClients.filter(client => client.id != id)


      );

      }
    });
  }
}
