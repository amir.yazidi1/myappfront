import { Component, OnInit } from '@angular/core';
import {OrdersService} from "../../../services/orders.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-edit-order',
  templateUrl: './edit-order.component.html',
  styleUrls: ['./edit-order.component.css']
})
export class EditOrderComponent implements OnInit {
  formOrder: FormGroup;
  submitted = false;
  id;
  order;
  constructor(private OrderService: OrdersService, private fb: FormBuilder, private router: ActivatedRoute) {
    this.id = this.router.snapshot.params.id;

  }

  ngOnInit(): void {
    this.formOrder = this.fb.group({
      id: [''],
      name: ['', Validators.required],
      totalPrice: ['', Validators.required],
      description: ['', Validators.required]
    });
    this.getById()


  }

  get f() {
    return this.formOrder.controls;
  }

  updateOrder() {


    console.log(this.formOrder.value)
    this.submitted = true;
    if (this.formOrder.invalid) {
      return;
    }
    this.OrderService.putOrder(this.formOrder.value).subscribe()

  }

  async  getById() {
    await this.OrderService.getOrderBy(this.id).subscribe(res => {

      console.log("res : ", res["name"])
      this.order = res

      this.formOrder.setValue({
        id: this.id,
        name: res["name"],
        totalPrice: res["totalPrice"],
        description: res["description"],
      })
    })
  }
}
