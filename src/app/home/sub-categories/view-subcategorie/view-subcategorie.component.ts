import { Component, OnInit } from '@angular/core';
import {SubCatogoriesService} from "../../../services/sub-catogories.service";

@Component({
  selector: 'app-view-subcategorie',
  templateUrl: './view-subcategorie.component.html',
  styleUrls: ['./view-subcategorie.component.css']
})
export class ViewSubcategorieComponent implements OnInit {

  constructor(private subCatogoriesService: SubCatogoriesService) { }

  ngOnInit(): void {
  }

}
