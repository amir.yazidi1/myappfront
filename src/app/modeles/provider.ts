export class Provider {
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  image: string;
  campany: string;
}
