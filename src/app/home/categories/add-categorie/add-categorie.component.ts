import { Component, OnInit } from '@angular/core';
import {CategoriesService} from "../../../services/categories.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-categorie',
  templateUrl: './add-categorie.component.html',
  styleUrls: ['./add-categorie.component.css']
})
export class AddCategorieComponent implements OnInit {
  formCategory: FormGroup;
  submitted = false;
  constructor(private categoriesService: CategoriesService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formCategory = this.fb.group({
      name: ['', Validators.required],
      description: ['', Validators.required]
    });
  }
  get f() { return this.formCategory.controls; }
  AddCategory(){


    console.log(this.formCategory.value)
    this.submitted = true;
    if (this.formCategory.invalid) {return ; }
    this.categoriesService.postCategory(this.formCategory.value).subscribe()}
}
