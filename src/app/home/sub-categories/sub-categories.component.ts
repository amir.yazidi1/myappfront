import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {SubCatogoriesService} from "../../services/sub-catogories.service";
import {SubCategory} from "../../modeles/sub-category";

@Component({
  selector: 'app-sub-categories',
  templateUrl: './sub-categories.component.html',
  styleUrls: ['./sub-categories.component.css']
})
export class SubCategoriesComponent implements OnInit {
  subCategory:SubCategory[]

  constructor(private subCatogoriesService: SubCatogoriesService) { }

  ngOnInit(): void {
    this.getAllSubCategories();
  }

  getAllSubCategories(){

    this.subCatogoriesService.getSubCategoriesWS().subscribe(res => {
      this.subCategory = res;
    });
  }
  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {this.subCatogoriesService.deleteSubCategory(id).subscribe(
        () => {


          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.getAllSubCategories();

        }
        //  this.ListClients = this.ListClients.filter(client => client.id != id)


      );

      }
    });
  }
}
