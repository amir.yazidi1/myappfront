import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './auth/login/login.component';
import { RegisterComponent } from './auth/register/register.component';
import { ForgetPasswordComponent } from './auth/forget-password/forget-password.component';
import { ErrorComponent } from './error/error.component';
import { LayoutComponent } from './home/layout/layout.component';
import { FooterComponent } from './home/footer/footer.component';
import { HeaderComponent } from './home/header/header.component';
import { SideBarComponent } from './home/side-bar/side-bar.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './home/products/products.component';
import { OrdersComponent } from './home/orders/orders.component';
import { CategoriesComponent } from './home/categories/categories.component';
import { SubCategoriesComponent } from './home/sub-categories/sub-categories.component';
import { ProviderComponent } from './home/provider/provider.component';
import { ClientComponent } from './home/client/client.component';
import { AddClientComponent } from './home/client/add-client/add-client.component';
import { AddOrderComponent } from './home/orders/add-order/add-order.component';
import { AddProductComponent } from './home/products/add-product/add-product.component';
import { AddProviderComponent } from './home/provider/add-provider/add-provider.component';
import { AddCategorieComponent } from './home/categories/add-categorie/add-categorie.component';
import { AddSubCategorieComponent } from './home/sub-categories/add-sub-categorie/add-sub-categorie.component';
import { EditCategorieComponent } from './home/categories/edit-categorie/edit-categorie.component';
import { EditClientComponent } from './home/client/edit-client/edit-client.component';
import { EditOrderComponent } from './home/orders/edit-order/edit-order.component';
import { EditProductComponent } from './home/products/edit-product/edit-product.component';
import { EditProvidersComponent } from './home/provider/edit-providers/edit-providers.component';
import { EditSubCategorieComponent } from './home/sub-categories/edit-sub-categorie/edit-sub-categorie.component';
import { ViewCategorieComponent } from './home/categories/view-categorie/view-categorie.component';
import { ViewClientComponent } from './home/client/view-client/view-client.component';
import { ViewOrderComponent } from './home/orders/view-order/view-order.component';
import { ViewProductComponent } from './home/products/view-product/view-product.component';
import { ViewProviderComponent } from './home/provider/view-provider/view-provider.component';
import { ViewSubcategorieComponent } from './home/sub-categories/view-subcategorie/view-subcategorie.component';
import {HttpClient, HttpClientModule} from "@angular/common/http";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import { SearchPipe } from './pipes/search.pipe';
import { ToastrModule, ToastNoAnimation, ToastNoAnimationModule } from 'ngx-toastr';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ForgetPasswordComponent,
    ErrorComponent,
    LayoutComponent,
    FooterComponent,
    HeaderComponent,
    SideBarComponent,
    HomeComponent,
    ProductsComponent,
    OrdersComponent,
    CategoriesComponent,
    SubCategoriesComponent,
    ProviderComponent,
    ClientComponent,
    AddClientComponent,
    AddOrderComponent,
    AddProductComponent,
    AddProviderComponent,
    AddCategorieComponent,
    AddSubCategorieComponent,
    EditCategorieComponent,
    EditClientComponent,
    EditOrderComponent,
    EditProductComponent,
    EditProvidersComponent,
    EditSubCategorieComponent,
    ViewCategorieComponent,
    ViewClientComponent,
    ViewOrderComponent,
    ViewProductComponent,
    ViewProviderComponent,
    ViewSubcategorieComponent,
    SearchPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ToastNoAnimationModule.forRoot(),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
