import { Component, OnInit } from '@angular/core';
import {UserService} from "../../services/user.service";
import {Router} from "@angular/router";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  formClient: FormGroup;
  submitted = false;

  constructor(private userService:UserService,private router:Router, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formClient = this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }


  get f() {
    return this.formClient.controls;
  }

  login(){

     console.log("value : ",this.formClient.value)

    this.userService.login(this.formClient.value).subscribe(res=> {

      if(res!=null){
         localStorage.setItem("state","1")
        this.router.navigate([""])
      } else {

        alert("error ")
      }
    })

  }

}
