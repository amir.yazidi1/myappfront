import { Component, OnInit } from '@angular/core';
import {ProvidersService} from "../../../services/providers.service";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-add-provider',
  templateUrl: './add-provider.component.html',
  styleUrls: ['./add-provider.component.css']
})
export class AddProviderComponent implements OnInit {
  formProvider: FormGroup;
  submitted = false;
  constructor(private providerService: ProvidersService, private fb: FormBuilder) { }

  ngOnInit(): void {
    this.formProvider = this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      photo: ['', Validators.required],
      campany: ['', Validators.required]
    });
  }

  get f() { return this.formProvider.controls; }
  AddProvider(){


    console.log(this.formProvider.value)
    this.submitted = true;
    if (this.formProvider.invalid) {return ; }
    this.providerService.postProvider(this.formProvider.value).subscribe()}

}
