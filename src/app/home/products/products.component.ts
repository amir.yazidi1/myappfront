import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {ProductsService} from "../../services/products.service";
import {Product} from "../../modeles/product";

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
  products: Product[]
  constructor(private productsService: ProductsService) { }

  ngOnInit(): void {

    this.getAllProducts();
  }

  getAllProducts(){

    this.productsService.getProductsWS().subscribe(res => {
      this.products = res;
    });
  }
  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {this.productsService.deleteProduct(id).subscribe(
        () => {


          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.getAllProducts();

        }
        //  this.ListClients = this.ListClients.filter(client => client.id != id)


      );

      }
    });
  }

}
