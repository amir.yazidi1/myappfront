import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditSubCategorieComponent } from './edit-sub-categorie.component';

describe('EditSubCategorieComponent', () => {
  let component: EditSubCategorieComponent;
  let fixture: ComponentFixture<EditSubCategorieComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditSubCategorieComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditSubCategorieComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
