import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Category} from "../modeles/category";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {
  listCategorieShare: Category[];

  constructor(private http: HttpClient) { }

  getCategoriessWS(){
    return this.http.get<Category[]>( environment.baseUrl+"category/");
  }
  deleteCategory(id){
    return this.http.delete(environment.baseUrl+"category/?id=" + id);
  }
  postCategory(category: Category){
    return this.http.post(environment.baseUrl+"category/", category);
  }
  putCategory(category: Category){
    return this.http.put(environment.baseUrl+"category/", category);
  }
  getCategoryBy(id){
    return this.http.get(environment.baseUrl+"category/" + id);
  }
}
