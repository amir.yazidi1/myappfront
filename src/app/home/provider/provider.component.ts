import { Component, OnInit } from '@angular/core';
import Swal from "sweetalert2";
import {ProvidersService} from "../../services/providers.service";
import {Provider} from "../../modeles/provider";

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {
  provider:Provider[]

  constructor(private providersService: ProvidersService) { }

  ngOnInit(): void {
    this.getAllProviders();
  }

  getAllProviders(){

    this.providersService.getProviderWS().subscribe(res => {
      this.provider = res;
    });
  }
  delete(id){

    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
      if (result.isConfirmed) {this.providersService.deleteProvider(id).subscribe(
        () => {


          Swal.fire(
            'Deleted!',
            'Your file has been deleted.',
            'success'
          );

          this.getAllProviders();

        }
        //  this.ListClients = this.ListClients.filter(client => client.id != id)


      );

      }
    });
  }
}
