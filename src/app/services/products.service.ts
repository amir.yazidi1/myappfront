import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Product} from "../modeles/product";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  listProductsShare: Product[];
  constructor(private http: HttpClient) { }

  getProductsWS(){
    return this.http.get<Product[]>( environment.baseUrl+"Products/");
  }
  deleteProduct(id){
    return this.http.delete(environment.baseUrl+"Products/?id=" + id);
  }
  postProduct(product: Product){
    return this.http.post(environment.baseUrl+"Products/", product);
  }
  putProduct(product: Product){
    return this.http.put(environment.baseUrl+"Products/", product);
  }

  getProductBy(id){
    return this.http.get(environment.baseUrl+"Products/" + id);
  }
}
