export class User {
  id: number;
  firstName: string;
  LastName: string;
  phone: string;
  image: string;
}
